


## DIRECTIONS:

You are required to follow the checkpoints published on Moodle and you are to push your changes via version control as part of the assessment for this unit.  In every folder you have relevant folder names describing what you should put in them.  

## FOLDER STRUCTURE:

You will also note that there are two folders with a '0' as a prefix. These folders should be updated periodically WITH EVERY checkpoint. To give you an example; your 

first 'push' to Git should include 

- Any work you did on the research 
- Any work you did on your prototype / implementation (if applicable)
- The Literature Review (complete draft)

Second 'push' to Git should include 

- Any work you did on the research 
- Any work you did on your prototype / implementation (if applicable)
- Any Updates to the Literature Review (after review by mentor)
- The Methodology Review (complete draft)

You should proceed in this manner. 

## SUBMISSIONS:

Please adhere to the guidelines and regulations that are provided to you in the root dolder.  You should read them accordingly when attempting a particular milestone. Any milestones that are not adhered to will lead to loss in marks related to the IT Project Unit. If you submit work that is not up to standard as per guidelines it will be regarded as NOT ATTEMPTED.

